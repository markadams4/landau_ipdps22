#!/bin/bash
#SBATCH -A m1759
#SBATCH -C gpu
#SBATCH -q regular
#SBATCH -t 0:30:00
#SBATCH -N 1
#SBATCH --ntasks-per-node=20
##SBATCH -c 20
#SBATCH -G 1
##SBATCH --exclusive

EXTRA='-dm_landau_num_species_grid 1,1 -ts_max_steps 1000 -dm_landau_ion_masses 2 -dm_landau_ion_charges 1 -ts_dt .5 -dm_landau_thermal_temps 1,1 -dm_landau_n 1.,1 -dm_landau_device_type kokkos -dm_mat_type aijkokkos -dm_vec_type kokkos -dm_landau_Ez 0.05 -ex2_connor_e_field_units -ex2_test_type spitzer -ts_rtol 1e-3 -ts_adapt_dt_max .5'

date
NP=1
SMP=1
N=1
if true
then
   for Q in 2 3 4
   do
       for AMR in 0 1 2 3 4
       do
           let "N=${NP} * ${SMP}"
           echo "N=" ${N} ", AMR=" ${AMR} ", NP=" ${NP}
           srun -G 1 -n ${NP} ../../ex2 ${EXTRA} -petscspace_degree ${Q} -dm_landau_amr_levels_max ${AMR},${AMR} | tee out_spitzer_Q_${Q}_AMR_${AMR}_Kokkos_CPU.txt
       done
       AMR=1
       srun -G 1 -n ${NP} ../../ex2 ${EXTRA} -petscspace_degree ${Q} -dm_landau_amr_levels_max ${AMR},${AMR} -dm_landau_amr_post_refine 1 | tee out_spitzer_Q_${Q}_AMR_5_Kokkos_CPU.txt
   done
#   AMR=3
#   Q=3
#   srun -G 1 -n ${NP} ../../ex2 ${EXTRA} -petscspace_degree ${Q} -dm_landau_amr_levels_max ${AMR},${AMR} -dm_landau_amr_post_refine 2 | tee exact_out_spitzer_Q_${Q}_AMR_${AMR}_post_2_Kokkos_CPU.txt
else
    AMR=0
    for Q in 1 2 3 4
    do
        srun -G 1 -n ${NP} ../../ex2 ${EXTRA} -petscspace_degree ${Q} -dm_landau_amr_levels_max ${AMR},${AMR} -dm_landau_amr_post_refine 4 | tee out_spitzer_Q_${Q}_AMR_6_Kokkos_CPU.txt
        srun -G 1 -n ${NP} ../../ex2 ${EXTRA} -petscspace_degree ${Q} -dm_landau_amr_levels_max ${AMR},${AMR} -dm_landau_amr_post_refine 5 | tee out_spitzer_Q_${Q}_AMR_7_Kokkos_CPU.txt
    done
fi

date
