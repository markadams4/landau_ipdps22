#!/bin/bash
#SBATCH -A m1759
#SBATCH -C gpu
#SBATCH -q regular
#SBATCH -t 0:30:00
#SBATCH -N 1
#SBATCH --ntasks-per-node=20
##SBATCH -c 20
#SBATCH -G 8
#SBATCH --exclusive

EXTRA='-log_view'
GPULU='-pc_factor_mat_ordering_type rcm -pc_factor_mat_solver_type cusparseband -mat_no_inode -dm_landau_num_thread_teams 8'
# -c 1 --cpu-bind=threads
date
for NP in 1 2 4
do
    for SMP in 1
    do
        let "N=${NP} * ${SMP}"
        echo "N=" ${N} ", SMP=" ${SMP} ", NP=" ${NP}
        srun -G 1 -n ${NP} -c 2 --cpu-bind=cores --ntasks-per-core=1 ~/mps-wrapper.sh ../ex2-cu -dm_landau_device_type cuda   -dm_mat_type aijcusparse -dm_vec_type cuda   ${EXTRA}          | tee out11_${SMP}_${NP}_Cuda_CPU.txt
        srun -G 1 -n ${NP} -c 2 --cpu-bind=cores --ntasks-per-core=1 ~/mps-wrapper.sh ../ex2-cu -dm_landau_device_type cuda   -dm_mat_type aijcusparse -dm_vec_type cuda   ${EXTRA} ${GPULU} | tee out11_${SMP}_${NP}_Cuda_GPU.txt
        srun -G 1 -n ${NP} -c 2 --cpu-bind=cores --ntasks-per-core=1 ~/mps-wrapper.sh ../ex2-kok -dm_landau_device_type kokkos -dm_mat_type aijkokkos   -dm_vec_type kokkos ${EXTRA}          | tee out11_${SMP}_${NP}_Kokkos_CPU.txt
    done
    for SMP in 2
    do
        let "N=${NP} * ${SMP}"
        echo "N=" ${N} ", SMP=" ${SMP} ", NP=" ${NP}
        srun -G 1 -n ${N} -c 1 --cpu-bind=threads --ntasks-per-core=${SMP} ~/mps-wrapper.sh ../ex2-cu -dm_landau_device_type cuda   -dm_mat_type aijcusparse -dm_vec_type cuda   ${EXTRA}          | tee out11_${SMP}_${NP}_Cuda_CPU.txt
        srun -G 1 -n ${N} -c 1 --cpu-bind=threads --ntasks-per-core=${SMP} ~/mps-wrapper.sh ../ex2-cu -dm_landau_device_type cuda   -dm_mat_type aijcusparse -dm_vec_type cuda   ${EXTRA} ${GPULU} | tee out11_${SMP}_${NP}_Cuda_GPU.txt
        srun -G 1 -n ${N} -c 1 --cpu-bind=threads --ntasks-per-core=${SMP} ~/mps-wrapper.sh ../ex2-kok -dm_landau_device_type kokkos -dm_mat_type aijkokkos   -dm_vec_type kokkos ${EXTRA}          | tee out11_${SMP}_${NP}_Kokkos_CPU.txt
    done
done

for NP in 8
do
    for SMP in 1
    do
        let "N=${NP} * ${SMP}"
        echo "N=" ${N} ", SMP=" ${SMP} ", NP=" ${NP}
        srun -G 1 -n ${NP} -c 2 --cpu-bind=cores --ntasks-per-core=1 ~/mps-wrapper.sh ../ex2-cu -dm_landau_device_type cuda   -dm_mat_type aijcusparse -dm_vec_type cuda   ${EXTRA}          | tee out11_${SMP}_${NP}_Cuda_CPU.txt 
        srun -G 1 -n ${NP} -c 2 --cpu-bind=cores --ntasks-per-core=1 ~/mps-wrapper.sh ../ex2-cu -dm_landau_device_type cuda   -dm_mat_type aijcusparse -dm_vec_type cuda   ${EXTRA} ${GPULU} | tee out11_${SMP}_${NP}_Cuda_GPU.txt 
        srun -G 1 -n ${NP} -c 2 --cpu-bind=cores --ntasks-per-core=1 ~/mps-wrapper.sh ../ex2-kok -dm_landau_device_type kokkos -dm_mat_type aijkokkos   -dm_vec_type kokkos ${EXTRA}          | tee out11_${SMP}_${NP}_Kokkos_CPU.txt 
    done
    for SMP in 2
    do
        let "N=${NP} * ${SMP}"
        echo "N=" ${N} ", SMP=" ${SMP} ", NP=" ${NP}
        srun -G 1 -n ${N} -c 1 --cpu-bind=threads --ntasks-per-core=${SMP} ~/mps-wrapper.sh ../ex2-cu -dm_landau_device_type cuda   -dm_mat_type aijcusparse -dm_vec_type cuda   ${EXTRA}          | tee out11_${SMP}_${NP}_Cuda_CPU.txt 
        srun -G 1 -n ${N} -c 1 --cpu-bind=threads --ntasks-per-core=${SMP} ~/mps-wrapper.sh ../ex2-cu -dm_landau_device_type cuda   -dm_mat_type aijcusparse -dm_vec_type cuda   ${EXTRA} ${GPULU} | tee out11_${SMP}_${NP}_Cuda_GPU.txt 
        srun -G 1 -n ${N} -c 1 --cpu-bind=threads --ntasks-per-core=${SMP} ~/mps-wrapper.sh ../ex2-kok -dm_landau_device_type kokkos -dm_mat_type aijkokkos   -dm_vec_type kokkos ${EXTRA}          | tee out11_${SMP}_${NP}_Kokkos_CPU.txt 
    done
done
date
