To build, run and generate plots for the Cori data in section 6.3, first use this modules:

Currently Loaded Modulefiles:
  1) modules/3.2.11.4                                  9) craype-network-aries                             17) job/2.2.4-7.0.1.1_3.50__g36b56f4.ari
  2) esslurm                                          10) cray-libsci/19.06.1                              18) dvs/2.12_2.2.167-7.0.1.1_17.6__ge473d3a2
  3) cgpu/1.0                                         11) udreg/2.3.2-7.0.1.1_3.52__g8175d3d.ari           19) alps/6.6.58-7.0.1.1_6.22__g437d88db.ari
  4) gcc/8.3.0                                        12) ugni/6.0.14.0-7.0.1.1_7.54__ge78e5b0.ari         20) rca/2.2.20-7.0.1.1_4.65__g8e3fb5b.ari
  5) cuda/11.1.1                                      13) pmi/5.0.14                                       21) atp/2.1.3
  6) hpcsdk/20.11                                     14) dmapp/7.1.1-7.0.1.1_4.64__g38cf134.ari           22) PrgEnv-gnu/6.0.5
  7) openmpi/4.0.3                                    15) gni-headers/5.0.12.0-7.0.1.1_6.43__g3b1768f.ari  23) cmake/3.18.2
  8) craype/2.6.2                                     16) xpmem/2.2.20-7.0.1.1_4.23__g0475745.ari

* Configure PETSc with the two configuration files in this directory in the "petsc" directory as a normal PETSc build. Then 'cd src/ts/utils/dmplexlandau/tutorials', and run 'make ex2 PETSC_ARCH=arch-cori-gpu-opt-gcc ; mv ex2 ex2-cu' and 'make ex2 PETSC_ARCH=arch-cori-gpu-opt-kokkos-gcc ; mv ex2 ex2-kok' to produce the executables for run_tput.sh. Submit run_tput.sh in a directory below the current directory.

* run_tput.sh should generate several output files. run the python, equipped with pandas: python ../plot_tput_cori.py out11_*
This should generate the throughput and comportment plots used in the paper in the Cori section 6.3.

