#!/usr/bin/env python3
# For Summit
# Usage: plot_tput out10_NC_NSMP_[Kokkos|Cuda]_*.txt
#
#import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as np
import sys,os,ntpath
import pandas as pd
solve_time = np.zeros( (4, 3, 5) )
newt_sec = np.zeros( (4, 3, 5) )
time_1_1 = np.zeros( (6, 4) )

np_idx_arr = [-1, 0,  1,  2,  -1, 3, -1, 4]
user_args = sys.argv[1:]
nr = 6
for filename in sys.argv[1:]:
    print(filename)
    base = filename.split('.')
    parts = base[0].split('_')
    smp = int(parts[1])
    nc = int(parts[2])
#    solver_type = parts[3];
#    lu_device = parts[4];
    be_idx = 0
    if solver_type == 'Fugaku':
        be_idx = 3;
        np_idx = 0
        smp_idx = 0
    else:
        if solver_type == 'Cuda': be_idx = 0
        if solver_type == 'Kokkos': be_idx = 1;
        if lu_device == 'GPU' : be_idx = be_idx + 2
        np_idx = np_idx_arr[nc]
        smp_idx = smp - 1
    np = smp*nc
    for text in open(filename,"r"):
        words = text.split()
        n = len(words)
        if n > 1 and words[1] == 'Solve:':
            tm = float(words[2])
            solve_time[be_idx][smp_idx][np_idx] = tm
            if (smp==1 and nc==1) or solver_type == 'Fugaku':
                time_1_1[0][be_idx] = tm # total time
#            print (base,smp,nc,tm)
        elif n > 2 and words[0] == 'Landau' and words[1] == 'Jacobian':
            nn = float(words[2])
            newt_sec[be_idx][smp_idx][np_idx] = (nr*np*nn)/(solve_time[be_idx][smp_idx][np_idx])
        elif n > 2 and words[0] == 'Landau' and words[1] == 'Operator':
            tm = float(words[4])
            if (smp==1 and nc==1) or solver_type == 'Fugaku':
                time_1_1[1][be_idx] = tm
        elif n > 2 and words[0] == 'Kernel' and int(words[2]) != 29:
            tm = float(words[4])
            if (smp==1 and nc==1) or solver_type == 'Fugaku':
                time_1_1[2][be_idx] = tm
        elif n > 2 and words[0] == 'MatLUFactorNum':
            tm = float(words[3])
            if (smp==1 and nc==1) or solver_type == 'Fugaku':
                time_1_1[3][be_idx] = tm
        elif n > 2 and words[0] == 'MatSolve':
            tm = float(words[3])
            if (smp==1 and nc==1) or solver_type == 'Fugaku':
                time_1_1[4][be_idx] = tm
        elif n > 2 and words[0] == 'Jac-f-df':
            try:             
                tm = float(words[4])
                if (smp==1 and nc==1):
                    time_1_1[5][be_idx] = tm
            except ValueError:
                print(ValueError)
#            print ('       np=', np, ', num=', nn, ' num tot = ', np*nn, ' time=',solve_time[be_idx][smp_idx][np_idx],' rate=', newt_sec[be_idx][smp_idx][np_idx])
pd.options.display.float_format = '{:,}'.format
#
smp_names = ['1','2','3']
np_names =  ['1','2','3',    '5',   '7']

dfc = pd.DataFrame(newt_sec[0][:][:], index=smp_names, columns=np_names)
dfc.index.name = 'process/core'
dfc.columns.name = 'cores/GPU'
print (dfc.to_latex(longtable=False, escape=False, float_format="{:,.0f}".format, caption='CUDA-10, CPU LU, V100 Newton iterations / sec', label='tab:cuda10'))

dfk = pd.DataFrame(newt_sec[1][:][:], index=smp_names, columns=np_names)
dfk.index.name = 'process/core'
dfk.columns.name = 'cores/GPU'
print (dfk.to_latex(longtable=False, escape=False, float_format="{:,.0f}".format, caption='Kokkos-CUDA-10, CPU LU, V100 iterations / sec', label='tab:kokkos10'))

dfk = pd.DataFrame(newt_sec[2][:][:], index=smp_names, columns=np_names)
dfk.index.name = 'process/core'
dfk.columns.name = 'cores/GPU'
print (dfk.to_latex(longtable=False, escape=False, float_format="{:,.0f}".format, caption='CUDA-10, GPU LU, V100 iterations / sec', label='tab:cuda10gpulu'))

arch_names = ['CUDA (CPU LU)','Kokkos-CUDA','CUDA (GPU LU)','Fugaku (normalized)' ]
part_names =  ['Total', 'Landau', '(Kernel)', 'factor', 'solve']

time_1_1[2][:] = time_1_1[2][:] + time_1_1[:][-1] # Add f,df/dx GPU time to "Kernel"
time_1_1.T[3][:] *= 10 # scale Fugaku data
dfp = pd.DataFrame(time_1_1[:][:-1].T, index=arch_names, columns=part_names)
#dfp = pd.DataFrame(10*time_1_1[:][:], index=arch_names, columns=part_names)
dfp.columns.name = 'Device'
print (dfp.to_latex(longtable=False, escape=False, float_format="{:,.1f}".format, caption='Component times -- V100/Power9 and Fugaku', label='tab:parts10'))
