#!/usr/bin/env python
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
    '--with-fc=mpifort',
    '--with-cc=mpicc',
    '--with-cxx=mpiCC',
    '--CFLAGS=-fPIC -g -DLANDAU_DIM=2 -DLANDAU_MAX_SPECIES=10 -DPETSC_HAVE_CUDA_ATOMIC -DLANDAU_MAX_Q=4',            
    '--CXXFLAGS=-fPIC -g -std=c++11 -DLANDAU_DIM=2 -DLANDAU_MAX_SPECIES=10 -DPETSC_HAVE_CUDA_ATOMIC -DLANDAU_MAX_Q=4',
    '--CUDAFLAGS=-g -Xcompiler -DLANDAU_DIM=2 -DLANDAU_MAX_SPECIES=10 -DPETSC_HAVE_CUDA_ATOMIC -DLANDAU_MAX_Q=4',
    '--FCFLAGS=-fPIC -g', 
    '--COPTFLAGS=-O3',
    '--CXXOPTFLAGS=-O3',
    '--with-ssl=0',
    '--with-batch=0',
    '--with-mpiexec=jsrun -g1',
    '--with-cuda=1',
    '--with-cudac=nvcc',
    '--with-cuda-gencodearch=70',
    '--download-p4est=1',
    '--download-zlib',
    '--with-blaslapack-lib=-L' + os.environ['OLCF_NETLIB_LAPACK_ROOT'] + '/lib64 -lblas -llapack',
    '--with-x=0',
    '--with-debugging=0',
    '--download-kokkos',
    '--download-kokkos-kernels',
    '--with-kokkos-kernels-tpl=0',
    '--with-make-np=8',
    'PETSC_ARCH=arch-summit-opt-gnu-kokkos-cuda'
  ]
  configure.petsc_configure(configure_options)
  
