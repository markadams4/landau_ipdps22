To build, run and generate plots for the Summit data in section 6.2, first use this modules:

Currently Loaded Modules:
  1) hsi/5.0.2.p5   2) xalt/1.2.1   3) lsf-tools/2.0   4) darshan-runtime/3.1.7   5) DefApps   6) cmake/3.18.2   7) gcc/6.4.0   8) spectrum-mpi/10.3.1.2-20200121   9) netlib-lapack/3.8.0  10) forge/20.0.1  11) cuda/10.1.243


* Configure PETSc with the two configuration files in this directory in the "petsc" directory as a normal PETSc build. Then 'cd src/ts/utils/dmplexlandau/tutorials', and run 'make ex2 PETSC_ARCH=arch-summit-opt-gpu-cuda ; mv ex2 ex2-cu' and 'make ex2 PETSC_ARCH=arch-summit-opt-gnu-kokkos-cuda ; mv ex2 ex2-kok' to produce the executables for run.bsub. Submit run.bsub in a directory below the current directory.

* run.bsub should generate several output files. run the python, equipped with pandas: python ../plot_tput_summit.py out_*
This should generate the throughput and comportment plots used in the paper in the Summit section 6.3.
The Fugaku data file is placed here for convenience and its generation is not automated.

