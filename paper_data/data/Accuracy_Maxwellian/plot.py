#!/usr/bin/env python3
# For Cori
# Usage: plot_tput out*.txt
#                  0     1    2   3             4
#
#import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as np
import sys,os,ntpath
import pandas as pd
error_number = np.zeros( (4, 8) )
exact = 1.6021928417810e+01

for filename in sys.argv[1:]:
    print (filename)
    base = filename.split('.')
    parts = base[0].split('_')
    Q = int(parts[3])
    AMR = int(parts[5])
    for text in open(filename,"r"):
        words = text.split()
        n = len(words)
        if n > 1 and words[1] == 'species-1:':
            tm = float(words[4])
            error_number[Q-1][AMR] = (tm-exact)/exact
        elif n > 12 and words[1] == 'Total:':
            nn = float(words[12])
            print ('num cells = ',nn)

#pd.options.display.float_format = '{:,}'.format
#pd.options.display.float_format = '${:,.2e}'.format
#pd.set_option('display.float_format', '{:.5f}'.format)
#pd.reset_option('display.float_format')
#
q_names =  ['Q1 (not SP) ', 'Q2', 'Q3', 'Q4']
amr_names =   ['0 (2x1)','1 (4x2)', '2 (14 cells)', '3 (20 cells)', '4 (26 cells)', '8x4', '16x16', '32x32']
#
dfc = pd.DataFrame(100*np.absolute(error_number[:][:]), index=q_names, columns=amr_names)
dfc.index.name = 'Order FE space'
dfc.columns.name = 'AMR levels'
dfc[dfc.eq(0)] = ''
print (dfc.to_latex(longtable=False, escape=False, float_format="{:,.4f}".format, caption='Relative error (\%) in integral of Maxwellian', label='tab:acc_maxwellian'))
