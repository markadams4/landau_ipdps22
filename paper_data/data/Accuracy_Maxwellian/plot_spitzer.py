#!/usr/bin/env python3
# For Cori
# Usage: plot_tput out*.txt
#                  0     1    2   3             4
#
#import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as np
import sys,os,ntpath
import pandas as pd
import seaborn as sns

error_number = np.zeros( (4, 8) )
exact = 1.6021928417810e+01

for filename in sys.argv[1:]:
    print (filename)
    base = filename.split('.')
    parts = base[0].split('_')
    Q = int(parts[3])
    AMR = int(parts[5])
    for text in open(filename,"r"):
        words = text.split()
        n = len(words)
        if n > 19 and words[0] == 'testSpitzer:':
            if words[20][0] != 'r':
                print (words[20])
                tm = float(words[20])
                error_number[Q-1][AMR] = tm - 1.0
        elif n > 12 and words[1] == 'Total:':
            nn = float(words[12])
            print ('num cells = ',nn)

#pd.options.display.float_format = '{:,}'.format
#pd.options.display.float_format = '${:,.2e}'.format
#pd.set_option('display.float_format', '{:.5f}'.format)
#pd.reset_option('display.float_format')
#
q_names =  ['Q1 (not SP)', 'Q2', 'Q3', 'Q4']
amr_names =       ['0 (2x1)','1 (4x2)', '2 (14 cells)', '3 (20 cells)', '4 (26 cells)', '8x4', '16x16', '32x32']
amr_names_short = ['(2x1)','(4x2)', '14 cells', '20 cells', '26 cells', '8x4', '16x16', '32x32']
#
print(error_number)
dfc = pd.DataFrame(100*np.absolute(error_number[:][:]), index=q_names, columns=amr_names)
dfc.index.name = 'Order FE space'
dfc.columns.name = 'AMR levels'
dfc[dfc.eq(0)] = ''
print (dfc.to_latex(longtable=False, escape=False, float_format="{:,.2f}".format, caption='Spitzer resistivity error (\%): 1 - $(E/J_{computed})$ / $\eta_{spitzer}$', label='tab:acc_spitzer'))

ax = sns.heatmap(100*np.absolute(error_number[:][:]), linewidth=0.5, xticklabels=amr_names_short, yticklabels=q_names, cmap="coolwarm", annot=True)
ax.set_title('Spitzer resistivity error (%)')
ax.set_ylabel('Q')
ax.set_xlabel('AMR')
#plt.show()
plt.savefig('sptizer_error_heatmap.png')
