masses:        e= 9.109e-31; ions in proton mass units:    2.000e+00  0.000e+00 ...
charges:       e=-1.602e-19; charges in elementary units:  1.000e+00  0.000e+00
thermal T (K): e= 1.160e+07 i= 1.160e+07  0.000e+00. v_0= 1.326e+07 ( 4.424e-02c) n_0= 1.000e+20 t_0= 5.787e-06, classical, Intuitive
Domain radius grid 0:  5.000e+00, 1:  8.252e-02
CalculateE j0=145647. Ec = 0.0509908
0 TS dt 0.5 time 0.
  0) species-0: charge density= -1.6150698525186e+01 z-momentum=  6.5490758751097e-21 energy=  2.3263228084426e+04
  0) species-1: charge density=  1.6150698525186e+01 z-momentum= -7.2123792731262e-19 energy=  2.3263228084426e+04
	  0) Total: charge density= -3.5527136788005e-15, momentum= -7.1468885143751e-19, energy=  4.6526456168851e+04 (m_i[0]/m_e = 3670.94, 20 cells)
testSpitzer J =  3.182e-09
testSpitzer:    0) time= 0.0000e+00 n_e=  1.008e+00 E=  2.550e-03 J=  3.182e-09 J_re=  4.677e-11 1.47% Te_kev=  9.886e-01 Z_eff=1. E/J to eta ratio= 4.70856e+13 (diff=-4.70756e+13) constant E normal
0) FormLandau: 360 IPs, 20 cells[0], Nb=9, Nq=9, dim=2, Tab: Nb=9 Nf=2 Np=9 cdim=2 N=178
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step   0 accepted t=0          + 5.000e-01 dt=5.500e-01  wlte=0.533  wltea=   -1 wlter=   -1
1 TS dt 0.55 time 0.5
testSpitzer J =  1.829e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step   1 accepted t=0.5        + 5.500e-01 dt=6.050e-01  wlte=0.223  wltea=   -1 wlter=   -1
2 TS dt 0.605 time 1.05
testSpitzer J =  3.531e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step   2 accepted t=1.05       + 6.050e-01 dt=6.655e-01  wlte=0.112  wltea=   -1 wlter=   -1
3 TS dt 0.6655 time 1.655
testSpitzer J =  5.126e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step   3 accepted t=1.655      + 6.655e-01 dt=7.321e-01  wlte=0.0674  wltea=   -1 wlter=   -1
4 TS dt 0.73205 time 2.3205
testSpitzer J =  6.619e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step   4 accepted t=2.3205     + 7.321e-01 dt=8.053e-01  wlte=0.0628  wltea=   -1 wlter=   -1
5 TS dt 0.805255 time 3.05255
testSpitzer J =  8.011e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step   5 accepted t=3.05255    + 8.053e-01 dt=8.858e-01  wlte=0.168  wltea=   -1 wlter=   -1
6 TS dt 0.885781 time 3.85781
testSpitzer J =  9.298e+04
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step   6 accepted t=3.85781    + 8.858e-01 dt=9.744e-01  wlte=0.0528  wltea=   -1 wlter=   -1
7 TS dt 0.974359 time 4.74359
testSpitzer J =  1.047e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step   7 accepted t=4.74359    + 9.744e-01 dt=1.000e+00  wlte=0.054  wltea=   -1 wlter=   -1
8 TS dt 1. time 5.71794
testSpitzer J =  1.153e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step   8 accepted t=5.71794    + 1.000e+00 dt=1.000e+00  wlte=0.054  wltea=   -1 wlter=   -1
9 TS dt 1. time 6.71794
testSpitzer J =  1.240e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step   9 accepted t=6.71794    + 1.000e+00 dt=1.000e+00  wlte=0.0538  wltea=   -1 wlter=   -1
10 TS dt 1. time 7.71794
testSpitzer J =  1.310e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  10 accepted t=7.71794    + 1.000e+00 dt=1.000e+00  wlte=0.0577  wltea=   -1 wlter=   -1
11 TS dt 1. time 8.71794
testSpitzer J =  1.365e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  11 accepted t=8.71794    + 1.000e+00 dt=1.000e+00  wlte=0.0756  wltea=   -1 wlter=   -1
12 TS dt 1. time 9.71794
testSpitzer J =  1.408e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  12 accepted t=9.71794    + 1.000e+00 dt=1.000e+00  wlte=0.255  wltea=   -1 wlter=   -1
13 TS dt 1. time 10.7179
testSpitzer J =  1.441e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  13 accepted t=10.7179    + 1.000e+00 dt=1.000e+00  wlte=0.115  wltea=   -1 wlter=   -1
14 TS dt 1. time 11.7179
testSpitzer J =  1.466e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  14 accepted t=11.7179    + 1.000e+00 dt=1.000e+00  wlte=0.369  wltea=   -1 wlter=   -1
15 TS dt 1. time 12.7179
testSpitzer J =  1.483e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  15 accepted t=12.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0858  wltea=   -1 wlter=   -1
16 TS dt 1. time 13.7179
testSpitzer J =  1.496e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  16 accepted t=13.7179    + 1.000e+00 dt=1.000e+00  wlte= 0.17  wltea=   -1 wlter=   -1
17 TS dt 1. time 14.7179
testSpitzer J =  1.503e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  17 accepted t=14.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0538  wltea=   -1 wlter=   -1
18 TS dt 1. time 15.7179
testSpitzer J =  1.507e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  18 accepted t=15.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0474  wltea=   -1 wlter=   -1
19 TS dt 1. time 16.7179
testSpitzer J =  1.508e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  19 accepted t=16.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0506  wltea=   -1 wlter=   -1
20 TS dt 1. time 17.7179
testSpitzer J =  1.507e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  20 accepted t=17.7179    + 1.000e+00 dt=1.000e+00  wlte=0.062  wltea=   -1 wlter=   -1
21 TS dt 1. time 18.7179
testSpitzer J =  1.505e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  21 accepted t=18.7179    + 1.000e+00 dt=1.000e+00  wlte=0.099  wltea=   -1 wlter=   -1
22 TS dt 1. time 19.7179
testSpitzer J =  1.501e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  22 accepted t=19.7179    + 1.000e+00 dt=1.000e+00  wlte=0.406  wltea=   -1 wlter=   -1
23 TS dt 1. time 20.7179
testSpitzer J =  1.496e+05
testSpitzer:   23) time= 2.0718e+01 n_e=  1.008e+00 E=  2.550e-03 J=  1.496e+05 J_re=  8.597e+00 0.00575% Te_kev=  9.610e-01 Z_eff=1. E/J to eta ratio= 0.959655 (diff=0.00115578) constant E normal
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  23 accepted t=20.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0819  wltea=   -1 wlter=   -1
24 TS dt 1. time 21.7179
testSpitzer J =  1.492e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  24 accepted t=21.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0505  wltea=   -1 wlter=   -1
25 TS dt 1. time 22.7179
testSpitzer J =  1.487e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  25 accepted t=22.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0424  wltea=   -1 wlter=   -1
26 TS dt 1. time 23.7179
testSpitzer J =  1.482e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  26 accepted t=23.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0421  wltea=   -1 wlter=   -1
27 TS dt 1. time 24.7179
testSpitzer J =  1.477e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  27 accepted t=24.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0467  wltea=   -1 wlter=   -1
28 TS dt 1. time 25.7179
testSpitzer J =  1.473e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  28 accepted t=25.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0581  wltea=   -1 wlter=   -1
29 TS dt 1. time 26.7179
testSpitzer J =  1.470e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  29 accepted t=26.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0862  wltea=   -1 wlter=   -1
30 TS dt 1. time 27.7179
testSpitzer J =  1.467e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  30 accepted t=27.7179    + 1.000e+00 dt=1.000e+00  wlte=0.213  wltea=   -1 wlter=   -1
31 TS dt 1. time 28.7179
testSpitzer J =  1.464e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  31 accepted t=28.7179    + 1.000e+00 dt=1.000e+00  wlte=0.166  wltea=   -1 wlter=   -1
32 TS dt 1. time 29.7179
testSpitzer J =  1.463e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  32 accepted t=29.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0645  wltea=   -1 wlter=   -1
33 TS dt 1. time 30.7179
testSpitzer J =  1.462e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  33 accepted t=30.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0381  wltea=   -1 wlter=   -1
34 TS dt 1. time 31.7179
testSpitzer J =  1.462e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  34 accepted t=31.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0261  wltea=   -1 wlter=   -1
35 TS dt 1. time 32.7179
testSpitzer J =  1.462e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  35 accepted t=32.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0193  wltea=   -1 wlter=   -1
36 TS dt 1. time 33.7179
testSpitzer J =  1.464e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  36 accepted t=33.7179    + 1.000e+00 dt=1.000e+00  wlte=0.015  wltea=   -1 wlter=   -1
37 TS dt 1. time 34.7179
testSpitzer J =  1.466e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  37 accepted t=34.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0121  wltea=   -1 wlter=   -1
38 TS dt 1. time 35.7179
testSpitzer J =  1.468e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  38 accepted t=35.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00999  wltea=   -1 wlter=   -1
39 TS dt 1. time 36.7179
testSpitzer J =  1.471e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  39 accepted t=36.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00843  wltea=   -1 wlter=   -1
40 TS dt 1. time 37.7179
testSpitzer J =  1.475e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  40 accepted t=37.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00724  wltea=   -1 wlter=   -1
41 TS dt 1. time 38.7179
testSpitzer J =  1.479e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  41 accepted t=38.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00631  wltea=   -1 wlter=   -1
42 TS dt 1. time 39.7179
testSpitzer J =  1.483e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  42 accepted t=39.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00558  wltea=   -1 wlter=   -1
43 TS dt 1. time 40.7179
testSpitzer J =  1.488e+05
testSpitzer:   43) time= 4.0718e+01 n_e=  1.008e+00 E=  2.550e-03 J=  1.488e+05 J_re=  2.053e+04 13.8% Te_kev=  9.242e-01 Z_eff=1. E/J to eta ratio= 0.910301 (diff=0.0043881) constant E normal
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 10
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  43 accepted t=40.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00499  wltea=   -1 wlter=   -1
44 TS dt 1. time 41.7179
testSpitzer J =  1.493e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  44 accepted t=41.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00451  wltea=   -1 wlter=   -1
45 TS dt 1. time 42.7179
testSpitzer J =  1.498e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  45 accepted t=42.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00411  wltea=   -1 wlter=   -1
46 TS dt 1. time 43.7179
testSpitzer J =  1.504e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
      TSAdapt basic arkimex 0:1bee step  46 accepted t=43.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00378  wltea=   -1 wlter=   -1
47 TS dt 1. time 44.7179
testSpitzer J =  1.509e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  47 accepted t=44.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0035  wltea=   -1 wlter=   -1
48 TS dt 1. time 45.7179
testSpitzer J =  1.515e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  48 accepted t=45.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00326  wltea=   -1 wlter=   -1
49 TS dt 1. time 46.7179
testSpitzer J =  1.521e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  49 accepted t=46.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00305  wltea=   -1 wlter=   -1
50 TS dt 1. time 47.7179
testSpitzer J =  1.527e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  50 accepted t=47.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00287  wltea=   -1 wlter=   -1
51 TS dt 1. time 48.7179
testSpitzer J =  1.533e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  51 accepted t=48.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00271  wltea=   -1 wlter=   -1
52 TS dt 1. time 49.7179
testSpitzer J =  1.539e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  52 accepted t=49.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00257  wltea=   -1 wlter=   -1
53 TS dt 1. time 50.7179
testSpitzer J =  1.545e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  53 accepted t=50.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00244  wltea=   -1 wlter=   -1
54 TS dt 1. time 51.7179
testSpitzer J =  1.550e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 9
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  54 accepted t=51.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00233  wltea=   -1 wlter=   -1
55 TS dt 1. time 52.7179
testSpitzer J =  1.556e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  55 accepted t=52.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00222  wltea=   -1 wlter=   -1
56 TS dt 1. time 53.7179
testSpitzer J =  1.562e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  56 accepted t=53.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00213  wltea=   -1 wlter=   -1
57 TS dt 1. time 54.7179
testSpitzer J =  1.567e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  57 accepted t=54.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00204  wltea=   -1 wlter=   -1
58 TS dt 1. time 55.7179
testSpitzer J =  1.572e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
      TSAdapt basic arkimex 0:1bee step  58 accepted t=55.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00196  wltea=   -1 wlter=   -1
59 TS dt 1. time 56.7179
testSpitzer J =  1.577e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  59 accepted t=56.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00188  wltea=   -1 wlter=   -1
60 TS dt 1. time 57.7179
testSpitzer J =  1.582e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 8
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  60 accepted t=57.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00181  wltea=   -1 wlter=   -1
61 TS dt 1. time 58.7179
testSpitzer J =  1.587e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  61 accepted t=58.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00175  wltea=   -1 wlter=   -1
62 TS dt 1. time 59.7179
testSpitzer J =  1.592e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  62 accepted t=59.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00169  wltea=   -1 wlter=   -1
63 TS dt 1. time 60.7179
testSpitzer J =  1.596e+05
testSpitzer:   63) time= 6.0718e+01 n_e=  1.008e+00 E=  2.550e-03 J=  1.596e+05 J_re=  3.117e+04 19.5% Te_kev=  9.170e-01 Z_eff=1. E/J to eta ratio= 0.838765 (diff=0.00225855) constant E normal
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  63 accepted t=60.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00163  wltea=   -1 wlter=   -1
64 TS dt 1. time 61.7179
testSpitzer J =  1.600e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  64 accepted t=61.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00158  wltea=   -1 wlter=   -1
65 TS dt 1. time 62.7179
testSpitzer J =  1.604e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  65 accepted t=62.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00153  wltea=   -1 wlter=   -1
66 TS dt 1. time 63.7179
testSpitzer J =  1.608e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  66 accepted t=63.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00148  wltea=   -1 wlter=   -1
67 TS dt 1. time 64.7179
testSpitzer J =  1.611e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  67 accepted t=64.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00143  wltea=   -1 wlter=   -1
68 TS dt 1. time 65.7179
testSpitzer J =  1.614e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  68 accepted t=65.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00139  wltea=   -1 wlter=   -1
69 TS dt 1. time 66.7179
testSpitzer J =  1.617e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 6
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 4
      TSAdapt basic arkimex 0:1bee step  69 accepted t=66.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00135  wltea=   -1 wlter=   -1
70 TS dt 1. time 67.7179
testSpitzer J =  1.620e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  70 accepted t=67.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00132  wltea=   -1 wlter=   -1
71 TS dt 1. time 68.7179
testSpitzer J =  1.623e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  71 accepted t=68.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00129  wltea=   -1 wlter=   -1
72 TS dt 1. time 69.7179
testSpitzer J =  1.626e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  72 accepted t=69.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00126  wltea=   -1 wlter=   -1
73 TS dt 1. time 70.7179
testSpitzer J =  1.628e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  73 accepted t=70.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00123  wltea=   -1 wlter=   -1
74 TS dt 1. time 71.7179
testSpitzer J =  1.630e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  74 accepted t=71.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0012  wltea=   -1 wlter=   -1
75 TS dt 1. time 72.7179
testSpitzer J =  1.632e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  75 accepted t=72.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00117  wltea=   -1 wlter=   -1
76 TS dt 1. time 73.7179
testSpitzer J =  1.634e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  76 accepted t=73.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00114  wltea=   -1 wlter=   -1
77 TS dt 1. time 74.7179
testSpitzer J =  1.636e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  77 accepted t=74.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00112  wltea=   -1 wlter=   -1
78 TS dt 1. time 75.7179
testSpitzer J =  1.637e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  78 accepted t=75.7179    + 1.000e+00 dt=1.000e+00  wlte=0.0011  wltea=   -1 wlter=   -1
79 TS dt 1. time 76.7179
testSpitzer J =  1.639e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  79 accepted t=76.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00107  wltea=   -1 wlter=   -1
80 TS dt 1. time 77.7179
testSpitzer J =  1.640e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  80 accepted t=77.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00105  wltea=   -1 wlter=   -1
81 TS dt 1. time 78.7179
testSpitzer J =  1.641e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  81 accepted t=78.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00103  wltea=   -1 wlter=   -1
82 TS dt 1. time 79.7179
testSpitzer J =  1.642e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  82 accepted t=79.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00101  wltea=   -1 wlter=   -1
83 TS dt 1. time 80.7179
testSpitzer J =  1.643e+05
testSpitzer:   83) time= 8.0718e+01 n_e=  1.008e+00 E=  2.550e-03 J=  1.643e+05 J_re=  3.233e+04 19.7% Te_kev=  9.193e-01 Z_eff=1. E/J to eta ratio= 0.817832 (diff=0.00027114) constant E normal
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  83 accepted t=80.7179    + 1.000e+00 dt=1.000e+00  wlte=0.00099  wltea=   -1 wlter=   -1
84 TS dt 1. time 81.7179
testSpitzer J =  1.644e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  84 accepted t=81.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000971  wltea=   -1 wlter=   -1
85 TS dt 1. time 82.7179
testSpitzer J =  1.644e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  85 accepted t=82.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000952  wltea=   -1 wlter=   -1
86 TS dt 1. time 83.7179
testSpitzer J =  1.645e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  86 accepted t=83.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000934  wltea=   -1 wlter=   -1
87 TS dt 1. time 84.7179
testSpitzer J =  1.645e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  87 accepted t=84.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000916  wltea=   -1 wlter=   -1
88 TS dt 1. time 85.7179
testSpitzer J =  1.646e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  88 accepted t=85.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000898  wltea=   -1 wlter=   -1
89 TS dt 1. time 86.7179
testSpitzer J =  1.646e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  89 accepted t=86.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000881  wltea=   -1 wlter=   -1
90 TS dt 1. time 87.7179
testSpitzer J =  1.647e+05
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 7
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
  Nonlinear solve converged due to CONVERGED_SNORM_RELATIVE iterations 5
      TSAdapt basic arkimex 0:1bee step  90 accepted t=87.7179    + 1.000e+00 dt=1.000e+00  wlte=0.000865  wltea=   -1 wlter=   -1
91 TS dt 1. time 88.7179
testSpitzer J =  1.647e+05
testSpitzer:   91) time= 8.8718e+01 n_e=  1.008e+00 E=  2.550e-03 J=  1.647e+05 J_re=  3.203e+04 19.4% Te_kev=  9.202e-01 Z_eff=1. E/J to eta ratio= 0.817098 (diff=-1.58828e-05) using Spitzer eta*J E transition
